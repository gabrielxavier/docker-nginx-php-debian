[![pipeline status](https://gitlab.com/gabrielxavier/docker-nginx-php-debian/badges/master/pipeline.svg)](https://gitlab.com/gabrielxavier/docker-nginx-php-debian/commits/master)

### WebServer with Nginx, PHP-FPM, Supervisord and Composer on Debian

#### Last versions of:

- Debian Buster
- Nginx 1.20
- PHP 5.6, 7.1, 7.2, 7.3, 7.4 and 8.0 (variable 'PHP_VER', default = 7.4)
- Supervisord 3.3
- Composer 1.10

        Volume: /var/www/html
        Port: 80

#### How to use
```shell
docker run -it -p 80:80 gabxav/webserver:latest
```
```shell
# phpinfo test page
http://127.0.0.1/index.php

# opcache test page
http://127.0.0.1/opcache.php

# session test page
http://127.0.0.1/session.php
```

#### Example
```shell
FROM gabxav/webserver:latest

RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bkp
RUN sed 's|/var/www/html|/var/www/html/public|' /etc/nginx/conf.d/default.conf.bkp > /etc/nginx/conf.d/default.conf

COPY ./src/ /var/www/html/

EXPOSE 80
```

#### [DockerHub Link](https://hub.docker.com/r/gabxav/webserver/)
